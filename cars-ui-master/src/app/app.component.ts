import { Component, Renderer2 } from '@angular/core';
import {adminservice} from './shared/services/admin/admin.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [adminservice]
})
export class AppComponent { 
  title = 'app';
  constructor(private renderer : Renderer2){
    this.renderer.addClass(document.body, 'm-home');
  }
}
