import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarmodificationComponent } from './carmodification.component';

describe('CarmodificationComponent', () => {
  let component: CarmodificationComponent;
  let fixture: ComponentFixture<CarmodificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarmodificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarmodificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
