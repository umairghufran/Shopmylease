import { Component, OnInit } from '@angular/core';
import { adminservice } from '../../../../shared/services/admin/admin.service';

@Component({
  selector: 'app-carmodification',
  templateUrl: './carmodification.component.html',
  styleUrls: ['./carmodification.component.css']
})
export class CarmodificationComponent implements OnInit {
  cars: any;
  listings: any;
  dealers: any;
  constructor(private adminservice: adminservice) {
    this.getCars();
    this.getListings();
    this.getDealers();
  }

  ngOnInit() {
  }


  getCars() {
    this.adminservice.getCars(data => {
      this.cars = data;

    });
  }
  getListings() {
    this.adminservice.getListings(data => {
      this.listings = data;

    });
  }
  getDealers() {
    this.adminservice.getDealers(data =>{
      this.dealers = data;
    });
  }

  setFeatured(carId: any, featured: boolean) {
    let data = {
      "id": carId,
      "featured": featured
    };

    this.adminservice.setFeatured(data);
    this.getCars();
  }

  setShow(carId: any, show: boolean) {
    let data = {
      "id": carId,
      "show": show
    };

    this.adminservice.setDisplay(data);
    this.getCars();
  }









}
