import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprroveDealersComponent } from './aprrove-dealers.component';

describe('AprroveDealersComponent', () => {
  let component: AprroveDealersComponent;
  let fixture: ComponentFixture<AprroveDealersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprroveDealersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprroveDealersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
