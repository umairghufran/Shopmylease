import { Component, OnInit } from '@angular/core';
import {adminservice} from '../../../../shared/services/admin/admin.service';

@Component({
  selector: 'app-adddealeradmin',
  templateUrl: './adddealeradmin.component.html',
  styleUrls: ['./adddealeradmin.component.css']
})
export class AdddealeradminComponent implements OnInit {
  signupsMsg:any;

  constructor(private adminservice:adminservice) { }

  ngOnInit() {
  }

  dealerSignupByAdmin(data){
    
    data = data.value;
    let self = this;

    let dealer = {
      first_name : data.firstname,
      last_name : data.lastname,
      email : data.email,
      password : data.password,
      businessname : data.businessname,
      mobile_no : data.mobile_no      
    }   

   this.adminservice.adddealeradmin(dealer,result =>{
    if(result.success === false){
      self.signupsMsg = {error: false , message : "Dealer added successfully"};
      console.log(result);

    } else if(result.success === true){
      self.signupsMsg = {error: true , message : "Error in adding a dealer. Please try again"};
    }
    
   });  
  }



  // {error: false , message : "Dealer signup successfully"}
  // {error: true , message : "Error in signup as dealer. Please try again"}
}
