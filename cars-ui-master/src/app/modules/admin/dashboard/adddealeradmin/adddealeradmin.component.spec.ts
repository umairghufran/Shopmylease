import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdddealeradminComponent } from './adddealeradmin.component';

describe('AdddealeradminComponent', () => {
  let component: AdddealeradminComponent;
  let fixture: ComponentFixture<AdddealeradminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdddealeradminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdddealeradminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
