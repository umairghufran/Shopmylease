import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminLoginComponent } from './login/login.component';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';  
import { AdminDashboardComponent } from './dashboard/dashboard.component';
import { AprroveDealersComponent } from './dashboard/aprrove-dealers/aprrove-dealers.component';
import { CarmodificationComponent } from './dashboard/carmodification/carmodification.component';
import { AdddealeradminComponent } from './dashboard/adddealeradmin/adddealeradmin.component';

var carsRoutes = [{
		path :'',
		children:[
		{
			path: 'login',
			component: AdminLoginComponent
		},{
			path: 'dashboard',
			component: AdminDashboardComponent
		}
		],
		component : AdminComponent
	}]
@NgModule({
  declarations: [
    AdminComponent,
    AdminLoginComponent,
		AdminDashboardComponent,
		AprroveDealersComponent,
		CarmodificationComponent,
		AdddealeradminComponent,
  ],
  imports: [
		FormsModule,
		RouterModule.forChild(carsRoutes),
		HttpModule,
		CommonModule
  ],
  exports:[RouterModule]
})
export class AdminModule { }