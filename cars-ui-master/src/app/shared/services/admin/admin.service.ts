import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import * as $ from 'jquery';
import 'rxjs/add/operator/map';


@Injectable()
export class adminservice {
    constructor() {

    }

    //  Get all Dealers 
    getDealers(cb): any {
        
     
        $.ajax({
            url: "http://localhost:3030/admin/displaydealers", success: function (result) {
                cb(result);
            }
        });
    }

    //  approval - Disapproval 

    // getCars
    getCars(cb): any {

         
        $.ajax({
            url: "http://localhost:3030/admin/displaymodels", success: function (result) {
                cb(result);

            }
        });
    }

    getListings(cb): any {

        $.ajax({
            url: "http://localhost:3030/admin/getlistings", success: function (result) {
                cb(result);

            }
        });
    }
  

    MakeApproval(data): any {
        $.ajax({
            type: "POST", url: "http://localhost:3030/admin/changeapproval", data, success: function (result) {
                console.log("/n change approval data" + result);

            }
        });
    }

    // setfeatured -  Car -  Home Listing 


    setFeatured(data): any {
        $.ajax({
            type: "POST", url: "http://localhost:3030/admin/carfeatured", data, success: function (result) {
                console.log("/n SetFeatured" + result);
            }
        });
        
    }
    setFeaturedListings(data): any {
        
        $.ajax({
            type: "POST", url: "http://localhost:3030/admin/changelistingfeatured", data, success: function (result) {
                console.log("/n SetFeatured" + result);
            }
        });
    }



    setDisplay(data): any {

        $.ajax({
            type: "POST", url: "http://localhost:3030/admin/cardisplay", data, success: function (result) {

                console.log("/n setdisplay" + result);
            }
        });
        

    }
    setDisplayListings(data): any {

        $.ajax({
            type: "POST", url: "http://localhost:3030/admin/changelistingshow", data, success: function (result) {

                console.log("/n setdisplay" + result);
            }
        });

    }


     //  

  adddealeradmin(data,cb):any{
    $.ajax({
        type: "POST", url: "http://localhost:3030/admin/adddealer", data, success: function (result) {
            cb(result);
            console.log(result);
        }
    });


  }







}
