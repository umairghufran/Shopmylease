var mongoose = require("mongoose");
var md5 = require("md5");

const Schema = mongoose.Schema;
var Admin = mongoose.model('Admin', new Schema({ 
    first_name: String,
    last_name: String,
    email : String, 
    password : String, 
    },{collection: 'admin'})
);


module.exports.find = function(data, cb){
    Admin.findOne({email: data.email }, function(err,admin){        
        if(err) {
            cb(err);
        }
        cb(false, admin);        
    });
}