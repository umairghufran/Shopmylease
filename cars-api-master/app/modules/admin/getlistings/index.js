var listings = require('../../../models/listing');

module.exports.getListings = function(req,res){
    listings.getListings(function(err, models){
        if(err){
        	res.send("Error in Getting listings data");            
        }
        else{
            res.json(models);
        }
    });
}