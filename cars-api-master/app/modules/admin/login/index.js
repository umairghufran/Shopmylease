var Admin = require('../../../models/admin');
var md5 = require("md5");
var jwtsign = require('jsonwebtoken');

module.exports.adminLogin = function(req,res){
	var data = req.body;
	Admin.find(data, function(err, admin){
		if(err){
			res.status(500).send(err);
		}
		else{
			console.log(admin , "admin data");
			if(admin && admin.password == md5(data.password)){
                //Generate token now
                var loginToken = jwtsign.sign({email : admin.email}, 'Car-Deals-2017shhhhHHHHH');
                res.send({error:false, token: loginToken, user: admin});
            }
            else{
            	res.status(401).send({ error:true , message : "Invalid credentials."});	
            }
            
		}
	})
}