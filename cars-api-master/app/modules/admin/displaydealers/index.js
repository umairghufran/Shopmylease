var dealer = require('../../../models/dealer');
module.exports.displayDealers = function(req,res){
    dealer.findForAdmin(function(err, dealers){
        if(err){
        	res.status(500).send("Error in Getting dealer data");            
        }
        else{
            res.json(dealers);
        }
    });
}