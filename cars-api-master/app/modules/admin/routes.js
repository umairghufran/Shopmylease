const router = require("express").Router();
const login = require('./login');
const addDealer = require('./adddealer');
const displayDealers =require('./displaydealers');
const changeapproval = require('./changeapproval');
const cardisplay = require('./cardisplay');
const carfeatured = require('./carfeatured');
const displaymodels = require('./displaymodels');
const getlistings = require('./getlistings');
const changelistings = require('./changelisting');

router.post('/login', login.adminLogin);
router.post('/adddealer',addDealer.addDealer);
//router.post('/changelistingfeatured',changelistings.changeListingFeatured);
//router.post('/changelistingshow',changelistings.changeListingShow);
router.post('/changeapproval',changeapproval.changeapproval);
router.post('/cardisplay',cardisplay.carDisplay);
router.post('/carfeatured',carfeatured.carFeatured);

router.get('/displaydealers',displayDealers.displayDealers);
router.get('/displaymodels',displaymodels.displayModels);
//router.get('/getlistings',getlistings.getListings);

module.exports = router;

