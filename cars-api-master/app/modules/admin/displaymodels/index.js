var car = require('../../../models/car');
module.exports.displayModels = function(req,res){
    car.findModelsForAdmin(function(err, models){
        if(err){
        	res.send("Error in Getting dealer data");            
        }
        else{
            res.json(models);
        }
    });
}