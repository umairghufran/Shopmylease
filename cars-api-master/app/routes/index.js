var router = require("express").Router();
router.use('/user', require("../modules/user/route"));
router.use('/dealer', require("../modules/dealer/routes")); //i didn't change this because we will have to to change the post request from the UI
router.use('/car', require("../modules/cars/routes"));
router.use('/listing', require("../modules/listing/routes"));
router.use('/enquiry', require("../modules/enquiry/route"));
router.use('/admin',require("../modules/admin/routes"));
module.exports = router;